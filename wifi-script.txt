Is Your Home Wifi Network Secure?

[Darin is working on her laptop, drinking a cup of coffee.]

Darin (voiceover): Is your home wifi network secure? Would you even know if your network is open to hackers?

[Show blinky-light router in cabinet. HackerX peeks out and slowly wraps his hands around the router.]

Darin (voiceover): The truth is that hackers are constantly attacking your home network from the Internet with automated tools.

[Show miniature robotic HackerX clones bouncing off of router.]

Darin: Let's begin with the brain of your home wifi network: your wifi router. [Darin holds up router and smiles.] The router is a low-powered computer with antennae and Ethernet ports [Darin plugs in an Ethernet cable.] which creates and manages your wifi network. The router connects via a wired connection [Darin points at cable.] to your Internet service provider such as your cable company.

If you are like most people, you probably weren't even aware that you can configure your wifi network. Even if you did configure your router, are you certain that you configured it securely? The various Wifi standards are awash with acronyms and tech terminology, [Show acronyms such as "WEP", "WPA", "WPA2-PSK", "802.11n", "MIMO", "channel", "SSDID" "2.4 GHz", "5 GHz"... floating around the screen] so few people really understand what all these options are.

Let's dig in to determine if your home network is properly setup and secure.

[Show: "Disclaimer: this advice is for home wifi networks. Corporate networks have separate configuration issues."]

[Show wifi network diagram with cartoon Darin holding laptop.]

Darin (voiceover): Your wifi router serves as a communications bridge between your Internet's wired connection and your local, wireless devices. These devices need to stay within 200 feet of the router to maintain their connections to the router. That's a much smaller range than cell phone towers. The wireless connection degrades as you move away from the router- that's just basic radio physics- so your connection will be slower and spottier the farther away your device is. Environmental factors such as thick walls, wireless phones, or microwave ovens can also reduce your wifi network's maximum speed.

[Show cartoon Darin moving away from router, resulting in weaker cartoon signal. Cartoon Darin frowns.]

Darin (voiceover): When you click to connect to a wifi network [show wifi network selector for Windows] based on the network's SSID- its name, your laptop must first decide which encryption strategy to use (there are many), then send a PSK- pre-shared key- encrypted for validation on the router. If the laptop sends the correct password, the router grants access to the network.

So, is your router configured for maximum security? Let's dig in to find out!

Home routers are configured via your web browser, but you will need to know the IP address or local DNS name of the device. Often, this can be found on a sticker on the router [show images of router stickers] including the credentials you will need to log in through the router's web interface. Common locations are listed here. [Show "http://192.168.1.1", "http://192.168.0.1", "http://router.local"] The router's address can also be found in your laptop's TCP/IP settings [Show image of settings with router IP address circled].

Once you navigate with your web browser to the proper address, the router may ask you for credentials. These credentials can either be found printed on the router itself or in the router's manual, which you can find online. [Show Darin entering credentials to log in.]

In case you make a mistake while configuring the router, or, if you forget the administrator password, the router can be reset to its factory defaults by pushing and holding a button on the router here, [Darin indicates reset button.] so you can't do any real permanent damage by changing these settings. Just make sure that all the users of the network are aware that there may be wifi outages while you change the settings.

One you login as the administator, look for firmware updates. The router's web interface may warn you that a firmware update is available. You should update as soon as possible as these updates can include security fixes. Since your router will often face the Internet and protect your internal network's devices, it is a prime target for hackers and spammers.

Next, look for the wireless settings. A modern wifi router probably supports 802.11a,b,g,n, and even ac. [Show these terms on screen.] These codes refer to iterative improvements in wifi speed and signal strength. Your router likely supports networks on both the 2.4 GHz and 5 Ghz radio spectra. It's fine to run both simultaneously since they don't interfere with each other. If you have both radio bands running, it could be worthwhile to try to connect from your laptop to the other network. Depending on how far you are from the router and how many other wifi networks there are running around you, one of the bands may provide a stronger, faster connection. You can test the speed of your connection for comparison purposes using speedtest.net [Show link and web application on Darin's laptop.]

One advanced wifi setting you may wish to change to improve your network's speed is called "Transit Power". In your wifi configuration, it's probably set to 100% which sprawls the network to its maximum coverage distance. It may sound counterintuitive, but having transmit power set to maximum can actually reduce your network's speed. Why? [Show wifi conflict diagram.] Other wifi networks around you may compete for the same radio frequencies, so, by reducing the power and strength of the emitted radio waves, your network may compete less with surrounding networks. This is especially true in smaller homes or apartments where maximum wifi coverage covers the neighbors' rooms unnecessarily. Try reducing the wifi power to the minimum acceptable coverage level- test with speedtest.net to check the results.

Security-wise, the best you can do right now is enable WPA2-PSK ("pre-shared key") which is adequate for home networks. If you see WEP or WPA (without "2") enabled or running in "compatibility mode", disable them unless you have particularly old devices on the network which do not support WPA2. WEP and WPA have been throughly cracked and are effectively equivalent to no security. If your settings indicate an "open" wifi network, that means that no authentication nor encryption are required to access the network. Anyone within range of the open wifi network can view your Internet traffic and use your Internet connection- [sarcastically] this is probably not what you intended. Make sure that you use a reasonably secure password that is not easily guessed.

Finally, you may be compromising your security by sharing the wifi password with too many people. When you share your password, you are granting users unlimited access to your internal network which includes your laptops, phones, printers, and smart speakers. A malicious user could use your password to access or even reconfigure some devices. To prevent this, activate your router's "Guest Network" feature. Guests connect to a separate network which is isolated from your devices and other guests. That means that guests can connect to the Internet but not to your devices. Configuring the guest network means assigning it a name and password. You can then freely share that name and password with anyone to whom you wish to grant Internet access without risking your own devices.

Regardless of what encryption you setup with your wifi network, a wifi network is still less secure than an in-home wired network since wifi uses radio waves. For example, even if the wifi traffic cannot be decrypted, your use of the wifi network can be detected potentially a kilometer away (with special equipment) and could be used by burglars to detect if you are home. Exploits for WPA2 do exist, so make sure your router is still supported by its vendor and that the router's firmware is up-to-date.

So, why do hackers want access to your router? [Show HackerX's gloves reaching for Darin's router. Darin whacks his hands away without looking at him.] Aside from stealing your online credentials, organized crime can benefit from using your router and connected devices as automated spam senders called "spambots" or "zombies". [Show HackerX eating from can of spam- he brings a spoon to his mouth- cut to remove spam from spoon- HackerX chews.] Of course, if your network is entirely wired via Ethernet cables, consider shutting off wifi altogether or using it only for guest access.

Let's review the router checklist:

* ensure router firmware is up-to-date - same as with updates for any device
* try both the 2.4 and 5 Ghz bands - the best band may vary across your wifi devices
* experiment with reducing transmit power - you may find an easy speed increase
* validate that WPA2-PSK only is configured - don't forget to use a password that can't be guessed
* offer a guest network to your guests - even if you trust them, they could be unwittingly bringing malware to your network
* prefer a wired Ethernet connection whenever possible - your router has Ethernet ports. You will get better, faster, and more consistent network performance from Ethernet cables.

[Final skit: HackerX is on Darin's fire escape with the window partially open. He is looking at a tablet and eyeing entry to the domicile. The camera shows HackerX checking his tablet to see if Darin is home, based on her wifi signal. He decides that she is not home and put his hands underneath the window in preparation to enter. Darin enters the frame, casually closing the window on HackerX's fingers. He screams and writhers silently in pain as Darin walks cheerfully away. End scene.]
