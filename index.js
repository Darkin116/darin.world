document.addEventListener('DOMContentLoaded', function()
			  {
			      //add trigger for card flipper
			      var card = document.getElementsByClassName('card')[0];
			      var titles = document.querySelectorAll('.pagetitle');
			      var titleDivs = document.querySelectorAll('.card .pagetitle div');
			      titles.forEach((element,i,a) =>
					     element.addEventListener( 'click', flipCard)
					    );

			      //hide all images until they are loaded in the carousel
			      document.querySelectorAll('.carousel img').forEach(
				  (img,i,a) =>
				      img.style.display = "none"
			      );
			      //add css gradient updater
			      cardtech = document.getElementsByClassName('cardtech')[0];
			      //setInterval(updateTechCardBackground, 100);

			      //add computer link to flip
			      document.querySelectorAll('.flip').forEach(
				  (element,i,a) => element.addEventListener('click',
									    flipCard)
			      );

			      //add handler to hide overlay video
			      var videocontainer = document.getElementById('overlay-video-container');
			      videocontainer.addEventListener('click',
							      hideRunwayVideo);
			      var video = document.getElementById('overlay-video');
			      videocontainer.addEventListener('transitionend',
							      function()
							      {
								  if(videocontainer.style.opacity == '0')
								  {
								      videocontainer.style.zIndex = '-1000';
								      
								  }
								  if(videocontainer.style.opacity == '1')
								  {
								      video.play();
								  }
							      });
			  });

window.addEventListener('load', function()
			{
			    //unhide images
			    document.querySelectorAll('.carousel img').forEach(
				(img,i,a) =>
				    img.style.display = ""
			      );

			    var carouselDiv = document.getElementById('carousel');
			    //add carousel
			    createCarousel(carouselDiv);

			    //resizeOverlayVideo();
			    window.addEventListener('resize', () => { 
				resizeOverlayVideo();
			    });			    
			    
			});


var backgroundStep = 0;
var cardtech;

function updateTechCardBackground()
{
    var midStep1 = backgroundStep;
    var midStep2 = Math.min(backgroundStep + 5, 120);
    cardtech.style.backgroundImage = 'url("electronic.jpg"),linear-gradient(to bottom, rgba(226,226,226,0) 0%, rgba(226,226,226,1) ' + midStep1 + '%, rgba(226,226,226,1) ' + midStep2 + '%, rgba(226,226,226,0) 100%)';
    backgroundStep += 3;
    if(backgroundStep > 120)
	backgroundStep = 0;
}

//https://codepen.io/desandro/pen/wjeBpp
function createCarousel(root) {
    var cells = root.getElementsByClassName('carousel-item');
    var cellCount = cells.length;
    var theta = 360/cellCount;
    var cellSize = 0;
    var selectedIndex = 0;
    
    //calculate maximum cell size
    for(var i=0; i<cellCount; i++)
    {
	var width = cells[i].clientWidth;
	if(width > cellSize)
	    cellSize = width + 50;
	cells[i].dataset.imgnum=i;
    }
    var radius = Math.round( ( cellSize / 2) / Math.tan( Math.PI / cellCount ) );
    for(var i=0; i<cellCount; i++)
    {
	var cell = cells[i];
	var cellAngle = theta * i;
	cell.style.transform = 'rotateY(' + cellAngle + 'deg) translateZ(' + radius + 'px)';
	cell.dataset.angle = cellAngle;
	cell.dataset.radius = radius;
	root.addEventListener('click', imageClick, true);	
    }

    function rotateCarousel()
    {
	var angle = theta * selectedIndex * -1;
	root.style.transform = 'translateZ(' + -radius + 'px) ' + 'rotateY(' + angle + 'deg)';
	zoomFrontImage();	
    }

    function originalTransformForCell(index)
    {
	var celldata = cells[index].dataset;
	return transformForYZ(celldata.radius, celldata.angle);
    }

    function transformForYZ(radius, angle)
    {
	var ry = 'rotateY(' + angle + 'deg)';
	var tz = 'translateZ(' + radius + 'px) ';
	return ry + tz;
    }

    function imageClick(event)
    {
	event.stopPropagation();
	var t = event.target;
	if (t.tagName.toUpperCase() != 'IMG')
	    return;
	var imgnum = t.dataset.imgnum;
	//if top image is clicked, rotate to the right
	if(imgnum % cellCount == imgnum)
	{
	    selectedIndex++;
	}
	else
	{
	    selectedIndex = imgnum;
	}
	rotateCarousel();
    }

    function zoomFrontImage()
    {
    	//increase size of image at front
	var frontImage = cells[selectedIndex % cellCount];
	for(var i = 0; i < cellCount ; i++)
	{
	    var cell = cells[i];
	    cell.style.transform = originalTransformForCell(i);
	}
	var frontBoost = 200;
	frontImage.style.transform = transformForYZ(parseInt(frontImage.dataset.radius) + frontBoost,
						    frontImage.dataset.angle);
	var onfrontAction = frontImage.dataset.onfront;
	if(onfrontAction)
	{
	    window[onfrontAction].call(window, frontImage);
	}
    }
}

function resizeOverlayVideo()
{
    return;
    var videocontainer = document.getElementById('overlay-video-container');
    var video = document.getElementById('overlay-video');
    var carousel = document.getElementById('carousel');


    var carouselStyle = getComputedStyle(carousel);
    var w = carouselStyle.getPropertyValue('width');
    var h = carouselStyle.getPropertyValue('height');
    video.style.width = w;
    video.style.height = h;
    videocontainer.style.width = w;
    videocontainer.style.height = h;
}

function showRunwayVideo(img)
{
    var videocontainer = document.getElementById('overlay-video-container');
    videocontainer.style.zIndex = '1000';
    videocontainer.style.opacity = '1';

    document.getElementById('carousel').style.opacity = '0';

    var video = document.getElementById('overlay-video');
    video.currentTime = 0;
 }

function hideRunwayVideo(event)
{
    if(event)
	event.preventDefault();
    var videocontainer = document.getElementById('overlay-video-container');

    var video = document.getElementById('overlay-video');
    video.pause();
    
    videocontainer.style.opacity = '0';
    document.getElementById('carousel').style.opacity = '1';    
}

function flipCard()
{
    var card = document.getElementsByClassName('card')[0];    
    card.classList.toggle('is-flipped');
    hideRunwayVideo();
}
